#!/usr/bin/env python

from threading import Thread
import rospy
from tf import TransformBroadcaster
from tf.transformations import quaternion_from_euler
import math
from darwin_gazebo.darwin import Darwin
from geometry_msgs.msg import Twist
from std_msgs.msg import Int32
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import TransformStamped


class WJFunc:
    """
    Walk Joint Function CPG style
    Provides parameterized sine wave functions as y=offset+scale*(in_offset+in_scale*x)
    """
    def __init__(self):
        self.offset=0
        self.scale=1
        self.in_offset=0
        self.in_scale=1

    def get(self,x):
        """ x between 0 and 1"""
        f=math.sin(self.in_offset+self.in_scale*x)
        return self.offset+self.scale*f

    def clone(self):
        z=WJFunc()
        z.offset=self.offset
        z.scale=self.scale
        z.in_offset=self.in_offset
        z.in_scale=self.in_scale
        return z

    def mirror(self):
        z=self.clone()
        z.offset*=-1
        z.scale*=-1
        return z

    def __str__(self):
        return "y=%f+%f*sin(%f+%f*x)"%(self.offset,self.scale,self.in_offset,self.in_scale)

class WFunc:
    """
    Multi-joint walk function for Darwin
    """
    def __init__(self,**kwargs):
        self.parameters={}

        self.parameters["swing_scale"]=0
        self.parameters["step_scale"]=0.3
        self.parameters["step_offset"]=0.55
        self.parameters["ankle_offset"]=0
        self.parameters["vx_scale"]=0.5
        self.parameters["vy_scale"]=0.5
        self.parameters["vt_scale"]=0.4

        for k,v in kwargs.items():
            self.parameters[k]=v

        self.generate()

    def generate(self):
        """
        Build CPG functions for walk-on-spot (no translation or rotation, only legs up/down)
        """
        # f1=THIGH1=ANKLE1=L=R in phase
        self.pfn={} # phase joint functions
        self.afn={} # anti phase joint functions

        #~ print f
        f1=WJFunc()
        f1.in_scale=math.pi
        f1.scale=-self.parameters["swing_scale"]
        self.pfn["j_ankle2_l"]=f1
        self.pfn["j_thigh1_l"]=f1

        # f2=mirror f1 in antiphase
        f2=f1.mirror()
        #~ f2=WJFunc()
        self.afn["j_ankle2_l"]=f2
        self.afn["j_thigh1_l"]=f2

        f3=WJFunc()
        f3.in_scale=math.pi
        f3.scale=self.parameters["step_scale"]
        f3.offset=self.parameters["step_offset"]
        self.pfn["j_thigh2_l"]=f3
        f33=f3.mirror()
        f33.offset+=self.parameters["ankle_offset"]
        self.pfn["j_ankle1_l"]=f33

        f4=f3.mirror()
        f4.offset*=2
        f4.scale*=2
        self.pfn["j_tibia_l"]=f4

        s2=0
        f5=f3.clone()
        f5.in_scale*=2
        f5.scale=s2
        self.afn["j_thigh2_l"]=f5


        f6=f3.mirror()
        f6.in_scale*=2
        f6.scale=f5.scale
        f6.offset+=self.parameters["ankle_offset"]
        self.afn["j_ankle1_l"]=f6

        f7=f4.clone()
        f7.scale=0
        self.afn["j_tibia_l"]=f7

        f8 = WJFunc();
        f8.in_scale=math.pi;
        f8.scale = 0.5;
        f8.offset = 0;
        self.pfn["j_shoulder_l"]= f8;

        f9 = WJFunc();
        f9.in_scale=math.pi;
        f9.scale = -0.3;
        f9.offset = -1.2;
        self.afn["j_low_arm_l"]= f9;

        f10 = WJFunc();
        f10.in_scale=math.pi;
        f10.scale = 0;
        f10.offset = 1.3;
        self.pfn["j_high_arm_l"]= f10;

        f11 = WJFunc();
        f11.in_scale=math.pi;
        f11.scale = -0.5;
        f11.offset = 0;
        self.afn["j_shoulder_l"]= f11;

        f12 = WJFunc();
        f12.in_scale=math.pi;
        f12.scale = 0.3;
        f12.offset = -1.2;
        self.pfn["j_low_arm_l"]= f12;

        f13 = WJFunc();
        f13.in_scale=math.pi;
        f13.scale = 0;
        f13.offset = 1.3;
        self.afn["j_high_arm_l"]= f13;

        self.forward=[f5,f6]

        self.generate_right()
        self.joints=self.pfn.keys()

        self.show()

    def generate_right(self):
        """
        Mirror CPG functions from left to right and antiphase right
        """
        l=[ v[:-2] for v in self.pfn.keys()]
        for j in l:
            self.pfn[j+"_r"]=self.afn[j+"_l"].mirror()
            self.afn[j+"_r"]=self.pfn[j+"_l"].mirror()

        self.pfn["j_high_arm_r"].offset = 1.3;
        self.afn["j_high_arm_r"].offset = 1.3;

    def get(self,phase,x,velocity):
        """ Obtain the joint angles for a given phase, position in cycle (x 0,1)) and velocity parameters """
        angles={}
        for j in self.pfn.keys():
            if phase:
                v=self.pfn[j].get(x)
                angles[j]=v
            else:
                angles[j]=self.afn[j].get(x)
        self.apply_velocity(angles,velocity,phase,x)
        return angles




    def show(self):
        """
        Display the CPG functions used
        """
        for j in self.pfn.keys():
            print(j,"p",self.pfn[j],"a",self.afn[j])


    def apply_velocity(self,angles,velocity,phase,x):
        """ Modify on the walk-on-spot joint angles to apply the velocity vector"""

        # VX
        v=velocity[0]*self.parameters["vx_scale"]
        d=(x*2-1)*v
        if phase:
            angles["j_thigh2_l"]+=d
            angles["j_ankle1_l"]+=d
            angles["j_thigh2_r"]+=d
            angles["j_ankle1_r"]+=d
        else:
            angles["j_thigh2_l"]-=d
            angles["j_ankle1_l"]-=d
            angles["j_thigh2_r"]-=d
            angles["j_ankle1_r"]-=d

        # VY
        v=velocity[1]*self.parameters["vy_scale"]
        d=(x)*v
        d2=(1-x)*v
        if v>=0:
            if phase:
                angles["j_thigh1_l"]-=d
                angles["j_ankle2_l"]-=d
                angles["j_thigh1_r"]+=d
                angles["j_ankle2_r"]+=d
            else:
                angles["j_thigh1_l"]-=d2
                angles["j_ankle2_l"]-=d2
                angles["j_thigh1_r"]+=d2
                angles["j_ankle2_r"]+=d2
        else:
            if phase:
                angles["j_thigh1_l"]+=d2
                angles["j_ankle2_l"]+=d2
                angles["j_thigh1_r"]-=d2
                angles["j_ankle2_r"]-=d2
            else:
                angles["j_thigh1_l"]+=d
                angles["j_ankle2_l"]+=d
                angles["j_thigh1_r"]-=d
                angles["j_ankle2_r"]-=d

        # VT
        v=velocity[2]*self.parameters["vt_scale"]
        d=(x)*v
        d2=(1-x)*v
        if v>=0:
            if phase:
                angles["j_pelvis_l"]=-d
                angles["j_pelvis_r"]=d
            else:
                angles["j_pelvis_l"]=-d2
                angles["j_pelvis_r"]=d2
        else:
            if phase:
                angles["j_pelvis_l"]=d2
                angles["j_pelvis_r"]=-d2
            else:
                angles["j_pelvis_l"]=d
                angles["j_pelvis_r"]=-d



class Walker:
    """
    Class for making Darwin walk
    """
    def __init__(self,darwin):
        self.darwin=darwin
        self.running=False

        self.velocity=[0,0,0]
        self.walking=False
        self.func=WFunc()

        #~ self.ready_pos=get_walk_angles(10)
        self.ready_pos=self.func.get(True,0,[0,0,0])

        self._th_walk=None

        self._sub_cmd_vel=rospy.Subscriber(darwin.ns+"cmd_vel",Twist,self._cb_cmd_vel,queue_size=1)
        #self._sub_action = rospy.Subscriber(darwin.ns + "action", Int32, self._cb_action, queue_size = 1)
        self._pub_odom=rospy.Publisher(darwin.ns+"odom",Odometry,queue_size=1)
        self.current_velocity=[0,0,0]
        self.start_odom()

    def _cb_action(self, msg):
        # catching action
        rospy.loginfo("action")
        self.start()
        self.set_velocity(0, 0, 0);
        self.step_first();



    def step_first(self):

        angles = {}
        angles["j_shoulder_r"] = -0.5;
        angles["j_high_arm_r"] = 1.3;
        angles["j_low_arm_r"] = 0.8;

        angles["j_shoulder_l"] = -angles["j_shoulder_r"] ;
        angles["j_high_arm_l"] = angles["j_high_arm_r"];
        angles["j_low_arm_l"] = -angles["j_low_arm_r"];

        angles["j_thigh1_r"] = 0.35;
        angles["j_thigh1_l"] = 0.35;
        angles["j_ankle2_r"] = 0.35;
        angles["j_ankle2_l"] = 0.35;

        angles["j_thigh2_r"] = -0.8;
        angles["j_tibia_r"] = 1.7;
        angles["j_tibia_l"] = -0.9;
        angles["j_ankle1_l"] = -0.3;
        angles["j_ankle1_r"] = 0.7;
       # angles["

        self.darwin.set_angles(angles);
        rospy.sleep(1);
        self.step_second();


    def step_second(self):
        speed_param = 30;

        n = 100;
        i = 0;

        angles = {}
        r = rospy.Rate(50);
        while i < n:
            persent = float(i) / n

            angles["j_thigh2_r"] = -0.8 - 0.2 * persent;
            angles["j_tibia_r"] = 1.7 - 1.4 * persent;
            angles["j_ankle1_r"] = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])));

            self.darwin.set_angles(angles);
            i += speed_param;

            r.sleep()
        rospy.sleep(0.5);
        self.init_walk();




    def _cb_cmd_vel(self,msg):
        """
        Catches cmd_vel and update walker speed
        """
        print("cmdvel",msg)
        vx=msg.linear.x
        vy=msg.linear.y
        vt=msg.angular.z
        self.start()
        self.set_velocity(vx,vy,vt)

    def init_walk(self):
        """
        If not there yet, go to initial walk position
        """
        rospy.loginfo("Going to walk position")
        if self.get_dist_to_ready()>0.02:
            self.darwin.set_angles_slow(self.ready_pos)

    def start(self):
        if not self.running:
            self.running=True
            self.init_walk()
            self._th_walk=Thread(target=self._do_walk)
            self._th_walk.start()
            self.walking=True

    def start_odom(self):
        self._th_odom=Thread(target=self._odom_upd)
        self._th_odom.start()

    def _odom_upd(self):
        cur_time=rospy.get_rostime()
        last_time=rospy.get_rostime()
        x=0
        y=0
        th=0
        odom_broadcaster=TransformBroadcaster()
        r=rospy.Rate(100)
        while not rospy.is_shutdown():
            #rospy.spinOnce()
            cur_time=rospy.get_rostime()
            vx=self.current_velocity[0]
            vy=self.current_velocity[1]
            vth=self.current_velocity[2]

            dt=(cur_time-last_time).to_sec()
            dx=(vx*math.cos(th)-vy*math.sin(th))*dt
            dy=(vx*math.sin(th)+vy*math.cos(th))*dt
            dth=vth*dt

            x+=dx
            y+=dy
            th+=dth

            q=quaternion_from_euler(0, 0, th)

            odom_trans=TransformStamped()
            odom_trans.header.stamp=cur_time
            odom_trans.header.frame_id="odom"
            odom_trans.child_frame_id="base_link"

            odom_trans.transform.translation.x=x
            odom_trans.transform.translation.y=y
            odom_trans.transform.translation.z=0.0
            odom_trans.transform.rotation=q

            odom_broadcaster.sendTransform((x, y, 0), q, cur_time, "base_link", "odom")

            odom=Odometry()
            odom.header.stamp=cur_time
            odom.header.frame_id="odom"

            odom.pose.pose.position.x=x
            odom.pose.pose.position.y=y
            odom.pose.pose.position.z=0.0
            odom.pose.pose.orientation.x = q[0]
            odom.pose.pose.orientation.y = q[1]
            odom.pose.pose.orientation.z = q[2]
            odom.pose.pose.orientation.w = q[3]

            odom.child_frame_id="base_link"
            odom.twist.twist.linear.x=vx
            odom.twist.twist.linear.y=vy
            odom.twist.twist.angular.z=vth

            self._pub_odom.publish(odom)

            last_time=cur_time
            r.sleep()

    def stop(self):
        if self.running:
            self.walking=False
            rospy.loginfo("Waiting for stopped")
            while not rospy.is_shutdown() and self._th_walk is not None:
                rospy.sleep(0.1)
            rospy.loginfo("Stopped")
            self.running=False

    def set_velocity(self,x,y,t):
        self.velocity=[x,y,t]


    def _do_walk(self):
        """
        Main walking loop, smoothly update velocity vectors and apply corresponding angles
        """
        r=rospy.Rate(100)
        rospy.loginfo("Started walking thread")
        func=self.func

        # Global walk loop
        n=50
        p=True
        i=0
        self.current_velocity=[0,0,0]
        while not rospy.is_shutdown() and (self.walking or i<n or self.is_walking()):
            if not self.walking:
                self.velocity=[0,0,0]
            if not self.is_walking() and i==0: # Do not move if nothing to do and already at 0
                self.update_velocity(self.velocity,n)
                r.sleep()
                continue
            x=float(i)/n
            angles=func.get(p,x,self.current_velocity)
            self.update_velocity(self.velocity,n)
            self.darwin.set_angles(angles)
            i+=1
            if i>n:
                i=0
                p=not p
            r.sleep()
        rospy.loginfo("Finished walking thread")

        self._th_walk=None

    def is_walking(self):
        e=0.02
        for v in self.current_velocity:
            if abs(v)>e: return True
        return False

    def rescale(self,angles,coef):
        z={}
        for j,v in angles.items():
            offset=self.ready_pos[j]
            v-=offset
            v*=coef
            v+=offset
            z[j]=v
        return z


    def update_velocity(self,target,n):
        a=3/float(n)
        b=1-a
        self.current_velocity=[a*t+b*v for (t,v) in zip(target,self.current_velocity)]

    def get_dist_to_ready(self):
        angles=self.darwin.get_angles()
        return get_distance(self.ready_pos,angles)



def interpolate(anglesa,anglesb,coefa):
    z={}
    joints=anglesa.keys()
    for j in joints:
        z[j]=anglesa[j]*coefa+anglesb[j]*(1-coefa)
    return z

def get_distance(anglesa,anglesb):
    d=0
    joints=anglesa.keys()
    if len(joints)==0: return 0
    for j in joints:
        d+=abs(anglesb[j]-anglesa[j])
    d/=len(joints)
    return d


if __name__=="__main__":
    rospy.init_node("walker")
    rospy.sleep(1)

    rospy.loginfo("Instantiating Darwin Client")
    darwin=Darwin()
    rospy.loginfo("Instantiating Darwin Walker")
    walker=Walker(darwin)

    rospy.loginfo("Darwin Walker Ready")
    while not rospy.is_shutdown():
        rospy.sleep(1)
