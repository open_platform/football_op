#!/usr/bin/env python

from threading import Thread
import rospy
import math
from darwin_gazebo.darwin import Darwin
from geometry_msgs.msg import Twist
from std_msgs.msg import Int32


class Kick:
    """
    Class for making Darwin walk
    """
    def __init__(self,darwin):
        self.darwin=darwin

        self.ready_pos = {}
        self.ready_pos["j_thigh1_r"] = 0
        self.ready_pos["j_thigh1_l"] = 0
        self.ready_pos["j_thigh2_r"] = -0.6
        self.ready_pos["j_thigh2_l"] = 0.6
        self.ready_pos["j_tibia_r"] = 1
        self.ready_pos["j_tibia_l"] = -1
        self.ready_pos["j_ankle1_r"] = 0.4
        self.ready_pos["j_ankle1_l"] = -0.4
        self.ready_pos["j_ankle2_r"] = 0
        self.ready_pos["j_ankle2_l"] = 0
        self.ready_pos["j_shoulder_r"] = -0.2
        self.ready_pos["j_shoulder_l"] = 0.2
        self.ready_pos["j_low_arm_r"] = 1.6
        self.ready_pos["j_low_arm_l"] = -1.6
        self.ready_pos["j_high_arm_r"] = 1.3
        self.ready_pos["j_high_arm_l"] = 1.3

        self.kick_force = 1;


        self._sub_kick_force = rospy.Subscriber(darwin.ns + "kick_force", Int32, self._cb_kick_force, queue_size = 1)
        self._sub_action = rospy.Subscriber(darwin.ns + "action", Int32, self._cb_action, queue_size = 1)

    def _cb_action(self, msg):
        # catching action
        rospy.loginfo("action")
        if msg.data == 1:
            self.step_first_right();
        elif msg.data == 2:
            self.step_first_left();
        elif msg.data == 3:
            self.lateral_right_first();
        elif msg.data == 4:
            self.lateral_left_first();
        elif msg.data == 5:
            self.heel_right_first();
        elif msg.data == 6:
            self.heel_left_first();
        elif msg.data == 7:
            self.forward_right_first();
        elif msg.data == 8:
            self.forward_left_first();
        #elif msg.data == -1:
            #self.rise_from_forward_fall();
        #elif msg.data == -2:
            #self.rise_from_backward_fall();

    def _cb_kick_force(self, msg):
        self.kick_force = msg.data



    def step_first_right(self):

        angles = {}
        angles["j_shoulder_r"] = -0.5;
        angles["j_high_arm_r"] = 1.3;
        angles["j_low_arm_r"] = 1.5;

        angles["j_shoulder_l"] = -angles["j_shoulder_r"] ;
        angles["j_high_arm_l"] = angles["j_high_arm_r"];
        angles["j_low_arm_l"] = -angles["j_low_arm_r"];

        angles["j_thigh1_r"] = 0.35;
        angles["j_thigh1_l"] = 0.35;
        angles["j_ankle2_r"] = 0.35;
        angles["j_ankle2_l"] = 0.35;

        angles["j_thigh2_r"] = -0.8;
        angles["j_tibia_r"] = 1.7;
        angles["j_tibia_l"] = -0.9;
        angles["j_ankle1_l"] = -0.3;
        angles["j_ankle1_r"] = 0.7;
       # angles["

        self.darwin.set_angles(angles);
        rospy.sleep(1);
        self.step_second_right();


    def step_second_right(self):
        speed_param = self.kick_force;

        n = 100
        i = 0

        angles = {}
        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles["j_thigh2_r"] = -0.8 - 0.2 * persent
            angles["j_tibia_r"] = 1.7 - 1.4 * persent
            angles["j_ankle1_r"] = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))

            angles["j_shoulder_r"] = -0.5 + 0.5 * persent
            angles["j_low_arm_r"] = 1.5 + 0.3 * persent

            angles["j_shoulder_l"] = 0.5 + 0.5 * persent
            angles["j_low_arm_l"] =  -1.5 - 0.3 * persent

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.5)
        self.ready_pos_right()

    def ready_pos_right(self):
        angles ={}

        speed_param = self.kick_force;
        n = 100
        i = 0

        angles = {}
        r = rospy.Rate(50);
        while i < n:
            persent = float(i) / n

            angles["j_thigh2_r"] = -1 + 0.5  * persent # angles["j_thigh2_r"] = -1 + 0.3 * persent
            angles["j_tibia_r"] = 0.3 + 1.1 * persent
            angles["j_ankle1_r"] = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))
            angles["j_thigh2_l"] = 0.6
            angles["j_tibia_l"] = -1
            angles["j_ankle1_l"]  = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))


            angles["j_shoulder_r"] = 0 - 0.8 * persent # angles["j_shoulder_r"] = 0 - 0.5 * persent
            angles["j_low_arm_r"] = 1.8 - 0.2 * persent # angles["j_low_arm_r"] = 1.8 - 0.3 * persent

            angles["j_shoulder_l"] = 1 - 0.8 * persent # angles["j_shoulder_l"] = 1 - 0.5 * persent
            angles["j_low_arm_l"] = -1.8 + 0.2 * persent # angles["j_low_arm_l"] = -1.8 + 0.3 * persent


            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.5)
        angles["j_thigh2_r"] = -0.6
        angles["j_tibia_r"] = 1
        angles["j_ankle1_r"] = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))
        angles["j_thigh1_r"] = 0;
        angles["j_thigh1_l"] = 0;
        angles["j_ankle2_r"] = 0;
        angles["j_ankle2_l"] = 0;
        self.darwin.set_angles(angles)
        rospy.sleep(0.5)




    def step_first_left(self):

        angles = {}
        angles["j_shoulder_r"] = -0.5;
        angles["j_high_arm_r"] = 1.3;
        angles["j_low_arm_r"] = 1.5;

        angles["j_shoulder_l"] = -angles["j_shoulder_r"] ;
        angles["j_high_arm_l"] = angles["j_high_arm_r"];
        angles["j_low_arm_l"] = -angles["j_low_arm_r"];

        angles["j_thigh1_r"] = -0.35;
        angles["j_thigh1_l"] = -0.35;
        angles["j_ankle2_r"] = -0.35;
        angles["j_ankle2_l"] = -0.35;

        angles["j_thigh2_l"] = 0.8;
        angles["j_tibia_l"] = -1.7;
        angles["j_tibia_r"] = 0.9;
        angles["j_ankle1_r"] = 0.3;
        angles["j_ankle1_l"] = -0.7;
       # angles["

        self.darwin.set_angles(angles);
        rospy.sleep(1);
        self.step_second_left();


    def step_second_left(self):
        speed_param = self.kick_force;

        n = 100
        i = 0

        angles = {}
        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles["j_thigh2_l"] = 0.8 + 0.2 * persent # angles["j_thigh2_l"] = 0.8 + 0.2 * persent
            angles["j_tibia_l"] = -1.7 + 1.4 * persent # angles["j_tibia_l"] = -1.7 + 1.4 * persent
            angles["j_ankle1_l"] = (3.14/2 -(3.14 - angles["j_tibia_l"] - (3.14/2 + angles["j_thigh2_l"])))

            angles["j_shoulder_l"] = 0.5 - 0.5 * persent
            angles["j_low_arm_l"] = -1.5 - 0.3 * persent

            angles["j_shoulder_r"] = -0.5 - 0.5 * persent
            angles["j_low_arm_r"] =  1.5 + 0.3 * persent

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.5)
        self.ready_pos_left()

    def ready_pos_left(self):
        angles ={}

        speed_param = self.kick_force;
        n = 100
        i = 0

        angles = {}
        r = rospy.Rate(50);
        while i < n:
            persent = float(i) / n

            angles["j_thigh2_l"] = 1 - 0.5 * persent # angles["j_thigh2_l"] = 1 - 0.3 * persent
            angles["j_tibia_l"] = -0.3 - 1.1 * persent
            angles["j_ankle1_l"] = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))
            angles["j_thigh2_r"] = -0.6
            angles["j_tibia_r"] = 1
            angles["j_ankle1_r"]  = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))


            angles["j_shoulder_l"] = 0 + 0.8 * persent # angles["j_shoulder_l"] = 0 + 0.5 * persent
            angles["j_low_arm_l"] = -1.8 + 0.2 * persent # angles["j_low_arm_l"] = -1.8 + 0.3 * persent

            angles["j_shoulder_r"] = -1 + 0.8 * persent # angles["j_shoulder_r"] = -1 + 0.5 * persent
            angles["j_low_arm_r"] = 1.8 - 0.2 * persent # angles["j_low_arm_r"] = 1.8 - 0.3 * persent


            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.5)
        angles["j_thigh2_l"] = 0.6
        angles["j_tibia_l"] = -1
        angles["j_ankle1_l"] = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))
        angles["j_thigh1_r"] = 0;
        angles["j_thigh1_l"] = 0;
        angles["j_ankle2_r"] = 0;
        angles["j_ankle2_l"] = 0;
        self.darwin.set_angles(angles)
        rospy.sleep(0.5)

    def lateral_right_first(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            ##
            angles["j_thigh1_r"] = 0.6 * persent
            #angles["j_thigh1_l"] = angles["j_thigh1_r"] / 2

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.1)
        ##
        #self.darwin.set_angles(angles)
        self.lateral_right_second()

    def lateral_right_second(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            ##
            angles["j_thigh1_l"] = -0.3 * persent

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.1)
        ##
        angles["j_thigh1_r"] = 0
        angles["j_thigh1_l"] = 0
        self.darwin.set_angles(angles)

    def lateral_left_first(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles["j_thigh1_l"] = -0.6 * persent

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.1)
        self.lateral_left_second()

    def lateral_left_second(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles["j_thigh1_r"] = 0.3 * persent

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.1)
        angles["j_thigh1_r"] = 0
        angles["j_thigh1_l"] = 0
        self.darwin.set_angles(angles)

    def heel_right_first(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        angles["j_tibia_r"] = 0.2 # knee straightening
        angles["j_thigh2_r"] = -0 # leg straightening
        angles["j_ankle1_r"] = 0 # ankle straightening
        angles["j_tibia_l"] = -0 # left knee straightening
        angles["j_thigh2_l"] = 0 # leg straightening
        angles["j_ankle1_l"] = 0 #ankle straightening
        angles["j_thigh1_l"] = -0.1 # body tilt
        self.darwin.set_angles(angles)

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            ##
            angles["j_tibia_r"] = 0.2 + 1.2 * persent # knee backward
            angles["j_thigh2_r"] = -persent # leg forward
            #angles["j_thigh1_l"] = -0.1 * persent # body tilt

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.2)
        ##
        self.heel_right_second()

    def heel_right_second(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            ##
            angles["j_tibia_r"] = 1.2 - 1.2 * persent # knee forward
            angles["j_thigh2_r"] = -(1 - 1.6 * persent) # leg backward
            angles["j_ankle1_r"] = 0.7 * persent # ankle backward

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.3)
        ##
        self.heel_right_third()

    def heel_right_third(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            ##
            angles["j_tibia_r"] = 0.6 * persent
            angles["j_thigh2_r"] = 0.6 - 1.2 * persent
            angles["j_ankle1_r"]  = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))
            angles["j_tibia_l"] = -0.6 * persent
            angles["j_thigh2_l"] = -0.6 + 1.2 * persent
            angles["j_ankle1_l"] = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))
            # angles["j_thigh1_l"] = 0.2 - 0.2 * persent
            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.5)
        ##
        # angles["j_thigh1_l"] = 0
        self.darwin.set_angles(self.ready_pos)


    def heel_left_first(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        angles["j_tibia_l"] = -0.2 # knee straightening
        angles["j_thigh2_l"] = 0 # leg straightening
        angles["j_ankle1_l"] = -0 # ankle straightening
        angles["j_tibia_r"] = 0 # left knee straightening
        angles["j_thigh2_r"] = -0 # leg straightening
        angles["j_ankle1_r"] = 0 #ankle straightening
        angles["j_thigh1_r"] = 0.1 # body tilt
        self.darwin.set_angles(angles)

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles["j_tibia_l"] = -0.2 - 1.2 * persent # knee backward
            angles["j_thigh2_l"] = persent # leg forward

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.2)
        self.heel_left_second()

    def heel_left_second(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles["j_tibia_l"] = -1.2 + 1.2 * persent # knee forward
            angles["j_thigh2_l"] = 1 - 1.6 * persent # leg backward
            angles["j_ankle1_l"] = -0.7 * persent # ankle backward

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.3)
        self.heel_left_third()

    def heel_left_third(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles["j_tibia_l"] = -0.6 * persent
            angles["j_thigh2_l"] = -0.6 + 1.2 * persent
            angles["j_ankle1_l"] = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))
            angles["j_tibia_r"] = 0.6 * persent
            angles["j_thigh2_r"] = 0.6 - 1.2 * persent
            angles["j_ankle1_r"]  = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))
            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.5)
        self.darwin.set_angles(self.ready_pos)

    def forward_right_first(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            tibiar1 = 1.2
            thigh2r1 = 1
            angles["j_thigh2_r"] = self.ready_pos["j_thigh2_r"] + (-self.ready_pos["j_thigh2_r"] - thigh2r1) * persent
            angles["j_thigh2_l"] = self.ready_pos["j_thigh2_l"] + (-self.ready_pos["j_thigh2_l"] - 0.2) * persent
            angles["j_tibia_r"] = self.ready_pos["j_tibia_r"] + (-self.ready_pos["j_tibia_r"] + tibiar1) * persent
            angles["j_tibia_l"] = self.ready_pos["j_tibia_l"] + (-self.ready_pos["j_tibia_l"]) * persent
            angles["j_ankle1_r"] = self.ready_pos["j_ankle1_r"] + (-self.ready_pos["j_ankle1_r"] + tibiar1 - thigh2r1) * persent
            angles["j_ankle1_l"] = self.ready_pos["j_ankle1_l"] + (-self.ready_pos["j_ankle1_l"] + angles["j_thigh2_l"]) * persent

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.1)
        angles["j_tibia_l"] = angles["j_tibia_l"] - 1.2
        angles["j_ankle1_l"] = angles["j_ankle1_l"] - 1.2
        self.darwin.set_angles(angles)
        rospy.sleep(0.5)
        self.forward_right_second(angles, tibiar1, thigh2r1);

    def forward_right_second(self, angles, tibiar1, thigh2r1):
        angles2 = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            #angles["j_thigh2_r"] = self.ready_pos["j_thigh2_r"] + (-self.ready_pos["j_thigh2_r"] - thigh2r1) * persent
            angles2["j_thigh2_l"] = angles["j_thigh2_l"] + (-angles["j_thigh2_l"] + thigh2r1) * persent
            #angles["j_tibia_r"] = self.ready_pos["j_tibia_r"] + (-self.ready_pos["j_tibia_r"] + tibiar1) * persent
            angles2["j_tibia_l"] = angles["j_tibia_l"] + (-angles["j_tibia_l"] - tibiar1) * persent
            #angles["j_ankle1_r"] = self.ready_pos["j_ankle1_r"] + (-self.ready_pos["j_ankle1_r"] + tibiar1 - thigh2r1) * persent
            angles2["j_ankle1_l"] = angles["j_ankle1_l"] + (-angles["j_ankle1_l"] - tibiar1 + thigh2r1) * persent

            self.darwin.set_angles(angles2)
            i += speed_param

            r.sleep()
        rospy.sleep(0.3)
        self.darwin.set_angles(self.ready_pos)

    def forward_left_first(self):
        angles = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            tibial1 = 1.2
            thigh2l1 = 1
            angles["j_thigh2_l"] = self.ready_pos["j_thigh2_l"] + (-self.ready_pos["j_thigh2_l"] + thigh2l1) * persent
            angles["j_thigh2_r"] = self.ready_pos["j_thigh2_r"] + (-self.ready_pos["j_thigh2_r"] + 0.2) * persent
            angles["j_tibia_l"] = self.ready_pos["j_tibia_l"] + (-self.ready_pos["j_tibia_l"] - tibial1) * persent
            angles["j_tibia_r"] = self.ready_pos["j_tibia_r"] + (-self.ready_pos["j_tibia_r"]) * persent
            angles["j_ankle1_l"] = self.ready_pos["j_ankle1_l"] + (-self.ready_pos["j_ankle1_l"] - tibial1 + thigh2l1) * persent
            angles["j_ankle1_r"] = self.ready_pos["j_ankle1_r"] + (-self.ready_pos["j_ankle1_r"] + angles["j_thigh2_r"]) * persent

            self.darwin.set_angles(angles)
            i += speed_param

            r.sleep()
        rospy.sleep(0.1)
        angles["j_tibia_r"] = angles["j_tibia_r"] + 1.2
        angles["j_ankle1_r"] = angles["j_ankle1_r"] + 1.2
        self.darwin.set_angles(angles)
        rospy.sleep(0.5)
        self.forward_left_second(angles, tibial1, thigh2l1);

    def forward_left_second(self, angles, tibial1, thigh2l1):
        angles2 = {}
        speed_param = self.kick_force
        n = 100
        i = 0

        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n

            angles2["j_thigh2_r"] = angles["j_thigh2_r"] + (-angles["j_thigh2_r"] - thigh2l1) * persent
            angles2["j_tibia_r"] = angles["j_tibia_r"] + (-angles["j_tibia_r"] + tibial1) * persent
            angles2["j_ankle1_r"] = angles["j_ankle1_r"] + (-angles["j_ankle1_r"] + tibial1 - thigh2l1) * persent

            self.darwin.set_angles(angles2)
            i += speed_param

            r.sleep()
        rospy.sleep(0.3)
        self.darwin.set_angles(self.ready_pos)

    def rise_from_forward_fall(self):
        angles = {}
        angles["j_shoulder_r"] = 1.35
        angles["j_shoulder_l"] = -1.35
        angles["j_ankle1_r"] = 0.8
        angles["j_ankle1_l"] = -0.8
        angles["j_tibia_r"] = 1.57
        angles["j_tibia_l"] = -1.57
        angles["j_thigh2_r"] = -1.57
        angles["j_thigh2_l"] = 1.57
        angles["j_high_arm_r"] = 1.57
        angles["j_high_arm_l"] = 1.57
        angles["j_low_arm_r"] = 0.7
        angles["j_low_arm_l"] = -0.7
        self.darwin.set_angles(angles)
        rospy.sleep(4)
        angles["j_ankle1_r"] = -1.5
        angles["j_ankle1_l"] = 1.5
        angles["j_low_arm_r"] = 0
        angles["j_low_arm_l"] = 0
        angles["j_shoulder_r"] = 1.57
        angles["j_shoulder_l"] = -1.57
        self.darwin.set_angles(self.ready_pos)
        angles2 = {}
        angles2["j_ankle1_r"] = 0.6
        angles2["j_ankle1_l"] = -0.6
        self.darwin.set_angles(angles2)
        rospy.sleep(4)
        self.darwin.set_angles(self.ready_pos)


    def rise_from_backward_fall(self):
        angles = {}
        angles["j_shoulder_r"] = -1.5
        angles["j_shoulder_l"] = 1.5
        angles["j_ankle1_r"] = 1
        angles["j_ankle1_l"] = -1
        angles["j_tibia_r"] = 1.2
        angles["j_tibia_l"] = -1.2
        angles["j_thigh2_r"] = -1.2
        angles["j_thigh2_l"] = 1.2
        angles["j_high_arm_r"] = 1.2
        angles["j_high_arm_l"] = 1.2
        self.darwin.set_angles(angles)
        rospy.sleep(5)
        angles["j_ankle1_r"] = 0
        angles["j_ankle1_l"] = 0
        angles["j_thigh2_r"] = 0
        angles["j_thigh2_l"] = 0
        angles["j_tibia_r"] = 0
        angles["j_tibia_l"] = 0
        angles["j_high_arm_r"] = 0
        angles["j_high_arm_l"] = 0
        self.darwin.set_angles(angles)
        rospy.sleep(3)
        self.darwin.set_angles(self.ready_pos)


if __name__=="__main__":
    rospy.init_node("kick")
    rospy.sleep(1)

    rospy.loginfo("Instantiating Darwin Client")
    darwin=Darwin()
    rospy.loginfo("Instantiating Darwin Walker")
    kick=Kick(darwin)

    rospy.loginfo("Darwin Walker Ready")
    while not rospy.is_shutdown():
        rospy.sleep(1)
