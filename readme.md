FootballOP library.

### Description

FootballOP library is a library to play football with a robot. Currently supported robot is Robotis OP2 robot.

### Build

Create a workspace, for example `~/catkin_ws`:
```
mkdir -p ~/catkin_ws/src && cd ~/catkin_ws/ && catkin_make
```

Clone this repository to the workspace `src` folder, for example `~/catkin_ws/src`:
```
git clone <url>
```

Install dependences:
```
chmod +x ~/catkin_ws/src/football_op/dependences.sh && sudo ~/catkin_ws/src/football_op/dependences.sh
```

Build catkin project (inside workspace):
```
catkin_make
```

### Run

Launch simulation in gazebo:
```
roslaunch darwin_gazebo darwin_gazebo.launch
```

Run an example from available examples (see `football_op/examples` directory), for example following ball example:
```
rosrun football_op football_op_follow_ball
```
