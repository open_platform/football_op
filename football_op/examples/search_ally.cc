#include <football_op/football_op.hh>

int main(int argc, char** argv) {
    ros::init(argc, argv, "football_op_search_ally");
    ros::NodeHandle nh{};
    auto marker = std::array<cv::Point2f, 4>{};
    const auto function = [&marker](fop::OP2& robot, const cv::Mat& trans) {
        fop::searchAlly(robot, trans, marker);
    };
    auto robot = fop::OP2{ nh, function, };
    while (ros::ok()) {
        ros::spinOnce();
    }

    return 0;
}
