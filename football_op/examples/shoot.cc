#include <football_op/football_op.hh>

int main(int argc, char** argv) {
    ros::init(argc, argv, "football_op_pass");
    ros::NodeHandle nh{};
    constexpr auto hLow = 80;
    constexpr auto sLow = 0;
    constexpr auto vLow = 0;
    constexpr auto hHigh = 160;
    constexpr auto sHigh = 50;
    constexpr auto vHigh = 50;
    const auto function = [](fop::OP2& robot, const cv::Mat& trans) {
        static auto status = int{0};
        cv::Point_<double> offset;
        double width;
        fop::detectGoal(trans, offset, width);
        fop::shootBall(
                    robot,
                    trans,
                    hLow, sLow, vLow, hHigh, sHigh, vHigh,
                    offset,
                    0.03,
                    status
        );
    };
    auto robot = fop::OP2{ nh, function, };
    while (ros::ok()) {
        ros::spinOnce();
    }

    return 0;
}
