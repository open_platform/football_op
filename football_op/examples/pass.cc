#include <football_op/football_op.hh>

int main(int argc, char** argv) {
    ros::init(argc, argv, "football_op_pass");
    ros::NodeHandle nh{};
    constexpr auto hLow = 80;
    constexpr auto sLow = 0;
    constexpr auto vLow = 0;
    constexpr auto hHigh = 160;
    constexpr auto sHigh = 50;
    constexpr auto vHigh = 50;
    const auto function = [](fop::OP2& robot, const cv::Mat& trans) {
        static auto status = int{0};
        std::array<cv::Point2d, 4> marker;
        fop::searchAlly(robot, trans, marker);
        cv::Point2d markerPosition;
        for (const auto& corner : marker) {
            markerPosition += corner;
        }
        markerPosition.x /= marker.size();
        markerPosition.y /= marker.size();
        const auto markerOffset = fop::calculateOffsetRad(
                    markerPosition,
                    cv::Point2d{ trans.cols / 2, trans.rows / 2, },
                    robot.horizontalFov(),
                    robot.pan(),
                    robot.verticalFov(),
                    robot.tilt()
        );
        fop::passBall(
                    robot,
                    trans,
                    hLow, sLow, vLow, hHigh, sHigh, vHigh,
                    markerOffset,
                    (marker[0] - marker[2]).y,
                    0.03,
                    0.3,
                    status
        );
    };
    auto robot = fop::OP2{ nh, function, };
    while (ros::ok()) {
        ros::spinOnce();
    }

    return 0;
}
