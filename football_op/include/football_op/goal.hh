#ifndef FOP_GOAL_HH_
#define FOP_GOAL_HH_

#include <opencv2/opencv.hpp>

namespace fop {

/*!
 * @brief detects goal in data
 * @param trans is the data from optical sensor
 * @param[out] offset is the offset relative to center of data matrix of found
 *         goal if any
 * @param[out] width is the width of found goal if any
 * @return true if goal was found and false else
 */
template <typename Precision>
bool detectGoal(
        const cv::Mat& trans,
        cv::Point_<Precision>& offset,
        Precision& width
);

} /// namespace fop

namespace fop {

template <typename Precision>
static double angle(
        cv::Point_<Precision> pt1,
        cv::Point_<Precision> pt2,
        cv::Point_<Precision> pt0
) {
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1 * dx2 + dy1 * dy2) / std::sqrt(
            (dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10
    );
}

template <typename Precision>
bool detectGoal(
        const cv::Mat& trans,
        cv::Point_<Precision>& offset,
        Precision& width
) {
    std::vector<std::vector<cv::Point_<Precision>>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::Mat binary;
    cv::inRange(
            trans,
            cv::Scalar{ 180, 180, 180, },
            cv::Scalar{ 255, 255, 255, },
            binary
    );
    cv::findContours(
            binary,
            contours,
            cv::RETR_EXTERNAL,
            cv::CHAIN_APPROX_SIMPLE
    );
    std::vector<cv::Point_<Precision>> approx;
    constexpr auto MAX = std::numeric_limits<int>::max();
    int maxX = -1, minX = MAX, sr;
    for (int i = 0; i < contours.size(); i++) {
        cv::Point_<Precision> p1(-1, -1), p2(-1, MAX), p3(MAX, MAX), p4(MAX, -1);
        cv::approxPolyDP(
                cv::Mat(contours[i]),
                approx,
                cv::arcLength(cv::Mat(contours[i]), true)*0.02,
                true
        );
        if (approx.size() >= 6 && approx.size() <= 12) {
             for (int k = 0; k < approx.size(); k++){
                if(approx[k].x > maxX) {
                    maxX = approx[k].x;
                }
                if(approx[k].x < minX) {
                    minX = approx[k].x;
                }
             }

            sr = (maxX - minX) / 2.0 + minX;
            int hi = 0;
            for (int k =0; k < approx.size(); k++) {
                if (approx[k].x < sr) {
                    if(approx[k].y > p1.y) {
                        p1 = approx[k];
                    }
                    if(approx[k].y < p2.y) {
                        p2 = approx[k];
                    }
                } else {
                    if(approx[k].y > p4.y) {
                        p4 = approx[k];
                    }
                    if(approx[k].y < p3.y) {
                        p3 = approx[k];
                    }
                }
                if (binary.size().height / 2.0 >= approx[k].y) {
                    hi++;
                }
            }

            bool allInit =
                    (p1.x == -1 && p1.y == -1) ||
                    (p2.x == -1 && p2.y == 5000) ||
                    (p3.x == 5000 && p3.y == 5000) ||
                    (p4.x == 5000 && p4.y == -1);
            double xCen = binary.size().width / 2.0;
            bool full = p1.x < xCen && p4.x > xCen;
            width = p3.x - p1.x;
            double angle1 = angle(p1, p3, p2);
            double angle2 = angle(p2, p4, p3);
            if(
                    std::fabs(angle1 + angle2) < 0.4 &&
                    hi >= 4 &&
                    !allInit &&
                    full
            ) {
                cv::Point_<Precision> goalPosition(
                    sr,
                    binary.size().height/2.0
                );
                cv::Point_<Precision> imageCenter(
                        binary.size().width/2.0,
                        binary.size().height/2.0
                );
                offset = goalPosition - imageCenter;
                return true;
            }
        }
    }
    return false;
}

} /// namespace fop

#endif /// FOP_GOAL_HH_
