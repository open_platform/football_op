#ifndef FOP_SEARCH_HH_
#define FOP_SEARCH_HH_

#include <filesystem>
#include <opencv2/opencv.hpp>

#include <football_op/util.hh>

namespace fop {

namespace detail {

/*!
 * @brief helper function @see detectBallColor with and without Precision& arg
 *
 * @tparam Fun is the function that should be applied to found ball info
 * @tparam Threshold is the type of threshold to be used @typically int
 * @param[in] trans is the image from the camera
 * @param hLow is the minimum value of hue channel of ball color
 * @param sLow is the minimum value of saturation channel of ball color
 * @param vLow is the minimum value of value channel of ball color
 * @param hHigh is the maximum value of hue channel of ball color
 * @param sHigh is the maximum value of saturation channel of ball color
 * @param vHigh is the maximum value of value channel of ball color
 * @return true if a ball was found and false else
 */
template <typename Fun, typename Threshold>
bool detectBallColorHelper(
        const cv::Mat& trans,
        Fun&& function,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh
);

} /// namespace detail

/*!
 * @brief detect a ball in a image using its color knowledge
 *
 * @tparam Threshold is the type of threshold to be used @typically int
 * @param[in] trans is the image from the camera
 * @param[out] ballPosition is the ball position in the image if found
 *         and not defined else
 * @param[out] ballImageRadius is the ball radius in the image if found
 *         and not defined else
 * @param hLow is the minimum value of hue channel of ball color
 * @param sLow is the minimum value of saturation channel of ball color
 * @param vLow is the minimum value of value channel of ball color
 * @param hHigh is the maximum value of hue channel of ball color
 * @param sHigh is the maximum value of saturation channel of ball color
 * @param vHigh is the maximum value of value channel of ball color
 * @return true if a ball was found and false else
 */
template <typename Threshold, typename Precision>
bool detectBallColor(
        const cv::Mat& trans,
        cv::Point_<Precision>& ballPosition,
        Precision& ballImageRadius,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh
) {
    const std::function lambdaBallInfo = [
            &ballPosition,
            &ballImageRadius
    ](
            cv::Point_<Precision> center,
            Precision radius
    ) {
        ballPosition = std::move(center);
        ballImageRadius = std::move(radius);
    };
    return detail::detectBallColorHelper(
            trans,
            std::move(lambdaBallInfo),
            hLow,
            sLow,
            vLow,
            hHigh,
            sHigh,
            vHigh
    );
}

/*!
 * @brief detect a ball in the image using its color knowledge
 *
 * @param[in] trans is the image from the camera
 * @param[out] ballPosition is the ball position in the image if found
 *         and not defined else
 * @param hLow is the minimum value of hue channel of ball color
 * @param sLow is the minimum value of saturation channel of ball color
 * @param vLow is the minimum value of value channel of ball color
 * @param hHigh is the maximum value of hue channel of ball color
 * @param sHigh is the maximum value of saturation channel of ball color
 * @param vHigh is the maximum value of value channel of ball color
 * @return true if a ball was found and false else
 */
template <typename Threshold, typename Precision>
bool detectBallColor(
        const cv::Mat& trans,
        cv::Point_<Precision>& ballPosition,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh
) {
    const std::function lambdaBallInfo = [
            &ballPosition
    ](
            cv::Point_<Precision> center,
            [[maybe_unused]] Precision radius
    ) {
        ballPosition = std::move(center);
    };
    return detail::detectBallColorHelper(
            trans,
            std::move(lambdaBallInfo),
            hLow,
            sLow,
            vLow,
            hHigh,
            sHigh,
            vHigh
    );
}

/*!
 * @brief search a color ball in front of robot
 *
 * @tparam Robot is the type of a robot
 * @tparam Threshold is the threshold type
 * @param robot is the robot to be controlled
 * @param trans is the image from the camera
 * @param[out] ballOffset is the found ball offset in radians
 * @param[out] ballImageRadius is the found ball radius in pixels
 * @param hLow is the minimum value of hue channel of ball color
 * @param sLow is the minimum value of saturation channel of ball color
 * @param vLow is the minimum value of value channel of ball color
 * @param hHigh is the maximum value of hue channel of ball color
 * @param sHigh is the maximum value of saturation channel of ball color
 * @param vHigh is the maximum value of value channel of ball color
 * @return true if ball is found and false else
 */
template <typename Robot, typename Threshold, typename Precision>
bool searchBallColorInFront(
        Robot& robot,
        const cv::Mat& trans,
        cv::Point_<Precision>& ballPosition,
        Precision& ballImageRadius,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh
);

namespace detail {

template <typename Fun, typename Threshold>
bool detectBallColorHelper(
        const cv::Mat& trans,
        Fun&& fun,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh
) {
    constexpr int thresh = 100;

    cv::Mat transCopy = trans.clone();
    cv::GaussianBlur(transCopy, transCopy, cv::Size(9, 9), 2, 2);

    cv::inRange(
            transCopy,
            cv::Scalar(hLow, sLow, vLow),
            cv::Scalar(hHigh, sHigh, vHigh),
            transCopy
    );

    cv::Canny(transCopy, transCopy, thresh, thresh*2);

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(
            transCopy,
            contours,
            hierarchy,
            cv::RETR_TREE,
            cv::CHAIN_APPROX_SIMPLE
    );

    if (contours.empty()) {
        return false;
    } else {
        cv::Point_<float> center;
        float radius;
        cv::minEnclosingCircle(contours[0], center, radius);
        std::forward<Fun>(fun)(center, radius);
        return true;
    }
    return false;
}

} /// namespace detail

template <typename Robot, typename Threshold, typename Precision>
bool searchBallColorInFront(
        Robot& robot,
        const cv::Mat& trans,
        cv::Point_<Precision>& ballPosition,
        Precision& ballImageRadius,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh
) {
    if (detectBallColor(
            trans,
            ballPosition,
            ballImageRadius,
            hLow,
            sLow,
            vLow,
            hHigh,
            sHigh,
            vHigh
    )) {
        return true;
    }
    static auto ballSearchAngle = robot.tilt();
    static constexpr auto ballSearchAngleIncrement = -0.035;
    static constexpr auto ballSearchAngleMin = -1.0;
    static constexpr auto ballSearchAngleEpsilon =
            std::abs(ballSearchAngleIncrement) / 2;
    if (std::abs(robot.tilt() - ballSearchAngle) <= ballSearchAngleEpsilon) {
        ballSearchAngle += ballSearchAngleIncrement;
        if (ballSearchAngle <= ballSearchAngleMin) {
            ballSearchAngle = 0;
        }
    }
    robot.tilt(ballSearchAngle);
    return false;
}

} /// namespace fop

#endif /// FOP_SEARCH_HH_
