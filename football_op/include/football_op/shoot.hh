#ifndef FOP_SHOOT_HH_
#define FOP_SHOOT_HH_

#include <football_op/move_object_to_target.hh>

namespace fop {

/*!
 * @brief shoots a ball to the goal
 * @param robot is the robot to shoot the ball
 * @param trans is data from optical sensor
 * @param hLow is the low hue threshold for ball detection
 * @param sLow is the low saturation threshold for ball detection
 * @param vLow is the low value threshold for ball detection
 * @param hHigh is the high hue threshold for ball detection
 * @param sHigh is the high saturation threshold for ball detection
 * @param vHigh is the high value threshold for ball detection
 * @param goalOffsetRad is the offset [rad] of a goal
 * @param ballRealRadius is the radius [m] of a ball
 * @param status is the state of algorithm, typically should be zero
 * @return true if ball was shoot and false else
 */
template <typename Robot, typename Threshold, typename Precision>
bool shootBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        const cv::Point_<Precision>& goalOffsetRad,
        const Precision& ballRealRadius,
        int& status
);

} /// namespace fop

namespace fop {

template <typename Robot, typename Threshold, typename Precision>
bool shootBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        const cv::Point_<Precision>& goalOffsetRad,
        const Precision& ballRealRadius,
        int& status
) {
    static constexpr Precision kickForce = 12;
    return moveBallToTarget(
        robot,
        trans,
        hLow,
        sLow,
        vLow,
        hHigh,
        sHigh,
        vHigh,
        goalOffsetRad,
        ballRealRadius,
        status,
        kickForce
    );
}

} /// namespace fop

#endif /// FOP_SHOOT_HH_
