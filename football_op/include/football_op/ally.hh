#ifndef FOP_ALLY_HH_
#define FOP_ALLY_HH_

#include <tuple>
#include <optional>

#include <opencv2/aruco.hpp>
#include <opencv2/xfeatures2d.hpp>

#include <football_op/template_surf.hh>

namespace fop {

/*!
 * @brief searches an ally marker on data with a robot
 * @param robot is the robot which should search an ally
 * @param trans is the data from optical sensor
 * @param[out] marker is the marker corners of a found ally marker if any
 * @return true if an ally was found and false else
 */
template <typename Robot, typename Precision>
bool searchAlly(
        Robot& robot,
        const cv::Mat& trans,
        std::array<cv::Point_<Precision>, 4>& marker
);

/*!
 * @brief searches an ally on data with a robot
 * @param robot is the robot which should search an ally
 * @param trans is the data from optical sensor
 * @param[out] heightPx is the found ally height [px] if any
 * @return true if an ally was found and false else
 */
template <typename Robot, typename Precision>
bool searchAlly(
        Robot& robot,
        const cv::Mat& trans,
        Precision& heightPx
);

/*!
 * @brief searches an ally on data with a robot using non-free algorithms
 * @param robot is the robot which should search an ally
 * @param trans is the data from optical sensor
 * @param[out] offset is the offset of a found ally [px] if any
 * @param[out] height is the height of a found ally [px] if any
 * @return true if an ally was found and false else
 */
template <typename Robot>
bool searchAllyNonFree(
        Robot& robot,
        const cv::Mat& trans,
        cv::Point2f& offset,
        float& height
);

} /// namespace fop

namespace fop {

template <typename Robot>
bool checkTiltAndPan(Robot& robot) {
    auto tiltOrPanNonZero = false;
    if (robot.tilt() != 0) {
        robot.tilt(0);
        tiltOrPanNonZero = true;
    }
    if (robot.pan() != 0) {
        robot.pan(0);
        tiltOrPanNonZero = true;
    }
    return !tiltOrPanNonZero;
}

template <typename Robot, typename Precision>
bool searchAlly(
        Robot& robot,
        const cv::Mat& trans,
        std::array<cv::Point_<Precision>, 4>& marker
) {
    if (!checkTiltAndPan(robot)) {
        return false;
    }

    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point_<Precision>>> markerCorners;
    std::vector<std::vector<cv::Point_<Precision>>> rejectedCandidates;
    cv::Ptr<cv::aruco::DetectorParameters> parameters =
            cv::aruco::DetectorParameters::create();
    cv::Ptr<cv::aruco::Dictionary> dictionary =
            cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::detectMarkers(
            trans,
            dictionary,
            markerCorners,
            markerIds,
            parameters,
            rejectedCandidates
    );

    if (markerIds.empty()) {
        return false;
    }
    for (auto i = 0u; i < marker.size(); i++) {
        marker[i] = markerCorners[0][i];
    }
    return true;
}

template <typename Robot, typename Precision>
bool searchAlly(
        Robot& robot,
        const cv::Mat& trans,
        cv::Point_<Precision>& position,
        const cv::Mat& templateTarget
) {
    /// bigger - less recognitions / smaller - more recognitions
    constexpr static Precision THRESHOLD = 0.96;
    constexpr static Precision FACTOR_STEP = 0.05;

    if (!checkTiltAndPan(robot)) {
        return false;
    }

    if (!templateTarget.data) {
        //! error
        return false;
    }

    /// image should be gray
    cv::Mat transGrey;
    cv::cvtColor(trans, transGrey, cv::COLOR_BGR2GRAY);

    /// image should be gray
    cv::Mat templateTargetGrey;
    cv::cvtColor(templateTarget, templateTargetGrey, cv::COLOR_BGR2GRAY);

    int max = 0;
    /// factor of the image size
    Precision factor = 1.2;
    /// center of match location
    cv::Point_<Precision> matchLoc;
    /// maximum matching value
    Precision maxVal;

    /// using different scales of template
    while (factor > 0.8) {
        cv::Mat matResult;
        cv::Mat matResizedTemplate;
        cv::resize(
                templateTargetGrey,
                matResizedTemplate,
                cv::Size(),
                factor,
                factor,
                cv::INTER_AREA
        );
        cv::matchTemplate(
                transGrey,
                templateTargetGrey,
                matResult,
                cv::TemplateMatchModes::TM_CCOEFF);
        Precision minVal;
        cv::Point_<Precision> minLoc;
        cv::Point_<Precision> maxLoc;
        cv::minMaxLoc(
                matResult,
                &minVal,
                &maxVal,
                &minLoc,
                &maxLoc,
                cv::Mat()
        );
        if (maxVal > max) {
            max = maxVal;
            matchLoc = maxLoc;
        }
        factor -= FACTOR_STEP;

    }
    /// if there is a recognition -> exiting with true
    if (maxVal >= THRESHOLD) {
        position = matchLoc;
        return true;
    }
    return false;
}

std::optional<std::tuple<
    cv::Ptr<cv::xfeatures2d::SURF>,
    cv::Ptr<cv::DescriptorMatcher>,
    cv::Mat,
    std::vector<cv::KeyPoint>,
    std::vector<cv::Point2f>,
    double,
    int
>> processSurfTemplate() {
    constexpr static double ALPHA = 3.6;
    constexpr static int BETA = 0;
    cv::Mat retTemplate;
    //! @see football_op/template_surf.hh
    const auto matTemplate =
            cv::Mat(300, 200, CV_8UC3, surfMatDataVector.data());
    if (!matTemplate.data) {
        return {};
    }
    //! increasing contrast for better key points finding
    retTemplate = cv::Mat::zeros(matTemplate.size(), matTemplate.type());
    for(int y = 0; y < matTemplate.rows; y++) {
        for(int x = 0; x < matTemplate.cols; x++) {
            for(int c = 0; c < matTemplate.channels(); c++) {
                /// alpha_ - contrast | beta_ - brightness
                retTemplate.at<cv::Vec3b>(y,x)[c] = cv::saturate_cast<uchar>(
                        ALPHA * matTemplate.at<cv::Vec3b>(y,x)[c] +
                        BETA
                );
            }
        }
    }
    //! bigger - less key points / smaller - more key points
    const int minHessian = 300;
    //! needs non free xfeatures2d library
    const auto retDetector = cv::xfeatures2d::SURF::create(minHessian);
    std::vector<cv::KeyPoint> keypointsTempl;
    cv::Mat descriptorsTempl;
    retDetector->detectAndCompute(
            retTemplate,
            cv::noArray(),
            keypointsTempl,
            descriptorsTempl
    );
    const auto retMatcher =
            cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
    auto cornersTempl = std::vector<cv::Point2f>(4);
    cornersTempl[0] = cv::Point(0, 0);
    cornersTempl[1] = cv::Point(retTemplate.cols, 0);
    cornersTempl[2] = cv::Point(retTemplate.cols, retTemplate.rows);
    cornersTempl[3] = cv::Point(0, retTemplate.rows);
    return {{
            retDetector,
            retMatcher,
            descriptorsTempl,
            keypointsTempl,
            cornersTempl,
            ALPHA,
            BETA,
    }};
}

template <typename Robot>
bool searchAllyNonFree(
        Robot& robot,
        const cv::Mat& trans,
        cv::Point2f& offset,
        float& height
) {
    static auto surfInit = processSurfTemplate();
    if (!static_cast<bool>(surfInit)) {
        //! error with template data
        return false;
    }
    const auto& [
            detector,
            matcher,
            descriptorsTempl,
            keypointsTempl,
            cornersTempl,
            alpha,
            beta
    ] = surfInit.value();
    //! robot is not watching forward ->
    //! skipping iteration and waiting for right angle
    if (!checkTiltAndPan(robot)) {
        return false;
    }
    //! contrast increase
    cv::Mat copy = cv::Mat::zeros(trans.size(), trans.type());
    for(int y = 0; y < trans.rows; y++) {
        for(int x = 0; x < trans.cols; x++) {
            for(int c = 0; c < trans.channels(); c++) {
                copy.at<cv::Vec3b>(y,x)[c] = cv::saturate_cast<uchar>(
                        alpha * trans.at<cv::Vec3b>(y,x)[c] +
                        beta
                );
            }
        }
    }
    std::vector<cv::KeyPoint> keypointsTrans;
    cv::Mat descriptorsTrans;
    detector->detectAndCompute(
            copy,
            cv::noArray(),
            keypointsTrans,
            descriptorsTrans
    );
    std::vector <std::vector <cv::DMatch> > matches;
    matcher->knnMatch(
            descriptorsTempl,
            descriptorsTrans,
            matches,
            2
    );
    //! bigger - more matches / smaller - less matches
    const float ratioThresh = 0.8;
    std::vector<cv::DMatch> goodMatches;
    for (int i = 0; i < matches.size(); i++) {
    if (matches[i][0].distance <
            ratioThresh * matches[i][1].distance) {
            goodMatches.push_back(matches[i][0]);
        }
    }
    constexpr static int minMatchesNum = 13;
    if (goodMatches.size() < minMatchesNum) {
        return false;
    }
    std::vector<cv::Point2f> templPoints;
    std::vector<cv::Point2f> transPoints;
    for (int i = 0; i < goodMatches.size(); i++) {
        templPoints.push_back(
                keypointsTempl[goodMatches[i].queryIdx].pt
        );
        transPoints.push_back(
                keypointsTrans[goodMatches[i].trainIdx].pt
        );
    }
    //! find the HOMOGRAPHY
    const cv::Mat H = cv::findHomography(templPoints, transPoints, cv::RANSAC);
    //! if there is no homography -> return false
    if (H.empty()) {
        return false;
    }
    //! else -> do perspective transform
    std::vector<cv::Point2f> transCorners(4);
    cv::perspectiveTransform(cornersTempl, transCorners, H);
    offset = cv::Point2f(
            ((transCorners[1].x + transCorners[0].x) / 2) -
                    copy.size().width / 2,
            ((transCorners[2].y + transCorners[1].y) / 2) -
                    copy.size().height / 2
    );
    height = transCorners[2].y - transCorners[1].y;
    return true;
}

} /// namespace fop

#endif /// FOP_ALLY_HH_
