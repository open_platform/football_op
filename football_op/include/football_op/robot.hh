#ifndef FOP_ROBOT_HH_
#define FOP_ROBOT_HH_

namespace fop {

class Robot {
public:
    virtual void velocity(double x, double y, double yaw) = 0;
    virtual void tilt(double angle) = 0;
    virtual void pan(double angle) = 0;
    virtual void kickLeft(int force) = 0;
    virtual void kickRight(int force) = 0;
    [[nodiscard]] virtual double focusLengthPx() const = 0;
    [[nodiscard]] virtual double horizontalFov() const = 0;
    [[nodiscard]] virtual double verticalFov() const = 0;
    [[nodiscard]] virtual double pan() const = 0;
    [[nodiscard]] virtual double tilt() const = 0;
    [[nodiscard]] virtual double footLength() const = 0;
    [[nodiscard]] virtual double footKickedLength() const = 0;
    [[nodiscard]] virtual double velocityLinearForwardMax() const = 0;
    [[nodiscard]] virtual double velocityLinearLateralMax() const = 0;
protected:
private:
};

} /// namespace fop

#endif /// FOP_ROBOT_HH_
