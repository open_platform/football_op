#ifndef FOP_PASS_HH_
#define FOP_PASS_HH_

#include <football_op/move_object_to_target.hh>

namespace fop {

/*!
 * @brief passes the ball to the ally
 * @param robot is the robot to pass the ball
 * @param trans is data from optical sensor
 * @param hLow is the low hue threshold for ball detection
 * @param sLow is the low saturation threshold for ball detection
 * @param vLow is the low value threshold for ball detection
 * @param hHigh is the high hue threshold for ball detection
 * @param sHigh is the high saturation threshold for ball detection
 * @param vHigh is the high value threshold for ball detection
 * @param allyOffsetRad is the offset [rad] of an ally
 * @param allyImageSize is the size (height) [px] of an ally in data matrix
 * @param ballRealRadius is the real size [m] of a ball
 * @param allyRealSize is the real size (height) [m] of an ally
 * @param status is the state of algorithm, typically should be zero
 * @return true if ball was passed and false else
 */
template <typename Robot, typename Threshold, typename Precision>
bool passBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        const cv::Point_<Precision>& allyOffsetRad,
        const Precision& allyImageSize,
        const Precision& ballRealRadius,
        const Precision& allyRealSize,
        int& status
);

} /// namespace fop

namespace fop {

template <typename Robot, typename Threshold, typename Precision>
bool passBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        const cv::Point_<Precision>& allyOffsetRad,
        const Precision& allyImageSize,
        const Precision& ballRealRadius,
        const Precision& allyRealSize,
        int& status
) {
  static constexpr Precision kickDistanceCoef = 1. / 20;
  const auto distanceToAlly = detail::calculateDistanceToObject(
      allyImageSize,
      allyRealSize,
      robot.focusLengthPx()
  );
  return moveBallToTarget(
      robot,
      trans,
      hLow,
      sLow,
      vLow,
      hHigh,
      sHigh,
      vHigh,
      allyOffsetRad,
      ballRealRadius,
      status,
      kickDistanceCoef * distanceToAlly
  );
}

} /// namespace fop

#endif /// FOP_PASS_HH_
