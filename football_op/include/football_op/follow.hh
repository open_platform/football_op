#ifndef FOP_FOLLOW_HH_
#define FOP_FOLLOW_HH_

#include <football_op/rectangle.hh>
#include <football_op/search.hh>

namespace fop {

/*!
 * @brief follows ball with a robot
 * @param robot is the robot to follow the ball
 * @param trans is data from optical sensor
 * @param hLow is the low hue threshold for ball detection
 * @param sLow is the low saturation threshold for ball detection
 * @param vLow is the low value threshold for ball detection
 * @param hHigh is the high hue threshold for ball detection
 * @param sHigh is the high saturation threshold for ball detection
 * @param vHigh is the high value threshold for ball detection
 * @param ballRealRadius is the radius [m] of a ball
 * @return true if ball was followed and false else
 */
template <typename Robot, typename Threshold, typename Precision>
bool followBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        Precision ballRealRadius
);

/*!
 * @brief follows ball with a robot and calculates ball position and radius
 * @param robot is the robot to follow the ball
 * @param trans is data from optical sensor
 * @param hLow is the low hue threshold for ball detection
 * @param sLow is the low saturation threshold for ball detection
 * @param vLow is the low value threshold for ball detection
 * @param hHigh is the high hue threshold for ball detection
 * @param sHigh is the high saturation threshold for ball detection
 * @param vHigh is the high value threshold for ball detection
 * @param ballRealRadius is the radius [m] of a ball
 * @param[out] ballPosition is the position relative to data matrix center
 *         of the found ball if any
 * @param[out] ballImageRadius is the radius [px] of the found ball if any
 * @return true if ball was followed and false else
 */
template <typename Robot, typename Threshold, typename Precision>
bool followBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        Precision ballRealRadius,
        cv::Point_<Precision>& ballPosition,
        Precision& ballImageRadius
);

} /// namespace fop

namespace fop {

template <typename Robot, typename Precision>
bool follow(
        Robot& robot,
        const cv::Point_<Precision>& target
) {
    //! allowed error in x direction
    constexpr static double BALL_ERROR_X = 0.03; //! [m]
    //! allowed error in y direction
    constexpr static double BALL_ERROR_Y = 0.05; //! [m]
    constexpr static double X_CRIT = 1. / 2; //! [m]
    constexpr static double Y_VEL_F = 0.3;
    constexpr static double YAW_VEL = 0.3;

    const Rectangle rect = Rectangle{
        BALL_ERROR_Y,
        BALL_ERROR_X + robot.footLength()
    };
    if (rect.valid(target.x, target.y)) {
        robot.velocity(0, 0, 0);
        return true;
    } else if (rect.valid(0, target.y)) {
        robot.velocity(
                0,
                target.x > 0 ?
                        -robot.velocityLinearLateralMax() :
                        robot.velocityLinearLateralMax(),
                0
        );
    } else if (rect.valid(target.x, 0)) {
        if (target.y > X_CRIT || target.y < -X_CRIT) {
            robot.velocity(
                    target.y > 0 ?
                            robot.velocityLinearForwardMax() :
                            -robot.velocityLinearForwardMax(),
                    0,
                    0
            );
        } else {
            robot.velocity(
                    robot.velocityLinearForwardMax() * target.y / X_CRIT +
                            target.y > 0 ?
                                    Y_VEL_F :
                                    -Y_VEL_F,
                    0,
                    0
            );
        }
    } else {
        robot.velocity(
                0,
                0,
                target.y < 0 ? +YAW_VEL : -YAW_VEL
        );
    }
    return false;
}

template <typename Robot, typename Threshold, typename Precision>
bool followBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        Precision ballRealRadius,
        cv::Point_<Precision>& ballPosition,
        Precision& ballImageRadius
) {
    const auto pan = robot.pan();
    const auto tilt = robot.tilt();
    if (searchBallColorInFront(
                robot,
                trans,
                ballPosition,
                ballImageRadius,
                hLow,
                sLow,
                vLow,
                hHigh,
                sHigh,
                vHigh
        )) {
        const auto imageCenter = cv::Point_<Precision>{
                trans.cols / 2,
                trans.rows / 2,
        };
        const auto focusLengthPx = robot.focusLengthPx();
        const auto horizontalFov = robot.horizontalFov();
        const auto verticalFov = robot.verticalFov();
        const auto offsetM = detail::calculateOffsetM(
                ballPosition,
                imageCenter,
                static_cast<decltype(ballRealRadius)>(ballImageRadius),
                ballRealRadius,
                focusLengthPx,
                horizontalFov,
                pan,
                verticalFov,
                tilt
        );
        if (follow(robot, detail::calculateOffsetM(
                ballPosition,
                imageCenter,
                static_cast<decltype(ballRealRadius)>(ballImageRadius),
                ballRealRadius,
                focusLengthPx,
                horizontalFov,
                pan,
                verticalFov,
                tilt
        ))) {
            return true;
        }
    } else {
        robot.velocity(0, 0, 0);
    }
    return false;
}

template <typename Robot, typename Threshold, typename Precision>
bool followBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        Precision ballRealRadius
) {
    cv::Point_<Precision> ballPosition;
    Precision ballImageRadius;
    return followBall(
            robot,
            trans,
            hLow,
            sLow,
            vLow,
            hHigh,
            sHigh,
            vHigh,
            ballRealRadius,
            ballPosition,
            ballImageRadius
    );
}

} /// namespace fop

#endif /// FOP_FOLLOW_HH_
