#ifndef FOP_AREA_HH_
#define FOP_AREA_HH_

namespace fop {

template <typename Precision>
class Area {
public:
    /*!
     * @brief checks if given point is inside of area
     * @param _x is the x coordinate of a point
     * @param _y is the y coordinate of a point
     * @return true if given point is inside of area and false else
     */
    virtual bool valid(const Precision _x, const Precision _y) const = 0;
protected:
private:
};

} /// namespace fop

#endif /// FOP_AREA_HH_
