#ifndef FOP_OP2_HH_
#define FOP_OP2_HH_

#include <football_op/robot.hh>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <sensor_msgs/JointState.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

namespace fop {

class OP2 : Robot {
public:
    explicit OP2(
            ros::NodeHandle nh,
            std::function<void(OP2& robot, const cv::Mat&)> function =
                [](auto, auto){},
            const std::string& robotId = "/darwin"
    );
    virtual void velocity(double x, double y, double yaw) override;
    virtual void tilt(double angle) override;
    virtual void pan(double angle) override;
    virtual void kickLeft(int force) override;
    virtual void kickRight(int force) override;
    [[nodiscard]] virtual double focusLengthPx() const override;
    [[nodiscard]] virtual double horizontalFov() const override;
    [[nodiscard]] virtual double verticalFov() const override;
    [[nodiscard]] virtual double pan() const override;
    [[nodiscard]] virtual double tilt() const override;
    [[nodiscard]] virtual double footLength() const override;
    [[nodiscard]] virtual double footKickedLength() const override;
    [[nodiscard]] virtual double velocityLinearForwardMax() const override;
    [[nodiscard]] virtual double velocityLinearLateralMax() const override;
protected:
    constexpr static auto TILT_OFFSET = 0.0;
    void jointStatesCb(const sensor_msgs::JointState& msg);
    void imageCb(const sensor_msgs::Image& msg);
    std::function<void(OP2& robot, const cv::Mat&)> function_;
    //! robot id uniquelly determines the robot to control
    std::string robotId_;
    ros::NodeHandle nh_;
    ros::Subscriber jointStatesSub_;
    ros::Subscriber imageSub_;
    ros::Publisher tiltPub_;
    ros::Publisher panPub_;
    ros::Publisher velPub_;
    ros::Publisher kickForcePub_;
    ros::Publisher kickActionPub_;
    double tilt_{0};
    double pan_{0};
private:
};

} /// namespace fop

namespace fop {

OP2::OP2(
        ros::NodeHandle nh,
        std::function<void(OP2& robot, const cv::Mat&)> function,
        const std::string& robotId
        ) : nh_{nh}, function_{function} {
    this->robotId_ = robotId;
    /// using robot namespace
    std::string darwinNamespace = robotId;
    this->jointStatesSub_ = this->nh_.subscribe(
                darwinNamespace + "/joint_states",
                100,
                &OP2::jointStatesCb,
                this
    );
    this->imageSub_ = this->nh_.subscribe(
                darwinNamespace + "/camera/image_raw",
                1,
                &OP2::imageCb,
                this
    );
    this->tiltPub_ = this->nh_.advertise<std_msgs::Float64>(
            darwinNamespace + "/j_tilt_position_controller/command", 100
    );
    this->panPub_ = this->nh_.advertise<std_msgs::Float64>(
            darwinNamespace + "/j_pan_position_controller/command", 100
    );
    this->velPub_ = this->nh_.advertise<geometry_msgs::Twist>(
            darwinNamespace + "/cmd_vel", 1
    );
    this->kickForcePub_ = this->nh_.advertise<std_msgs::Int32>(
            darwinNamespace + "/kick_force", 100
    );
    this->kickActionPub_ = this->nh_.advertise<std_msgs::Int32>(
            darwinNamespace + "/action", 100
    );
}

void OP2::velocity(double x, double y, double yaw) {
    geometry_msgs::Twist vel;
    vel.linear.x = x;
    vel.linear.y = y;
    vel.angular.z = yaw;
    this->velPub_.publish(vel);
}

void OP2::tilt(double angle) {
    std_msgs::Float64 angleMsg;
    angleMsg.data = angle + OP2::TILT_OFFSET;
    this->tiltPub_.publish(angleMsg);
}

void OP2::pan(double angle) {
    std_msgs::Float64 angleMsg;
    angleMsg.data = angle;
    this->panPub_.publish(angleMsg);
}

void OP2::kickLeft(int force) {
    std_msgs::Int32 kickForce;
    kickForce.data = force;
    this->kickForcePub_.publish(kickForce);
    std_msgs::Int32 kickBall;
    kickBall.data = 0;
    this->kickActionPub_.publish(kickBall);
    ros::Duration(2).sleep();
}

void OP2::kickRight(int force) {
    std_msgs::Int32 kickForce;
    kickForce.data = force;
    this->kickForcePub_.publish(kickForce);
    std_msgs::Int32 kickBall;
    kickBall.data = 1;
    this->kickActionPub_.publish(kickBall);
    ros::Duration(2).sleep();
}

double OP2::focusLengthPx() const {
    return 1900;
}

double OP2::horizontalFov() const {
    return 0.85;
}

double OP2::verticalFov() const {
    return this->horizontalFov() * 9 / 16;
}

double OP2::pan() const {
    return pan_;
}

double OP2::tilt() const {
    return tilt_ - OP2::TILT_OFFSET;
}

double OP2::footLength() const {
    return 0.12;
}

double OP2::footKickedLength() const {
    return this->footLength() * 2;
}

double OP2::velocityLinearForwardMax() const {
    return 0.8;
}

double OP2::velocityLinearLateralMax() const {
    return 0.6;
}

void OP2::jointStatesCb(
        const sensor_msgs::JointState& msg
) {
    const auto indexPan = std::distance(
        msg.name.begin(), std::find(msg.name.begin(), msg.name.end(), "j_pan")
    );
    const auto indexTilt = std::distance(
        msg.name.begin(), std::find(msg.name.begin(), msg.name.end(), "j_tilt")
    );
    this->pan_ = msg.position.at(indexPan);
    this->tilt_ = msg.position.at(indexTilt);
}

void OP2::imageCb(const sensor_msgs::Image& msg) {
    cv_bridge::CvImagePtr transPtr;
    transPtr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::RGB8);
    const auto& trans = transPtr->image;
    this->function_(*this, trans);
}

} /// namespace fop

#endif /// FOP_OP2_HH_
