#ifndef FOOTBALL_OP_HH_
#define FOOTBALL_OP_HH_

#include <football_op/op2.hh>
#include <football_op/search.hh>
#include <football_op/follow.hh>
#include <football_op/kick.hh>
#include <football_op/ally.hh>
#include <football_op/goal.hh>
#include <football_op/pass.hh>
#include <football_op/shoot.hh>

namespace fop {}

#endif /// FOOTBALL_OP_HH_
