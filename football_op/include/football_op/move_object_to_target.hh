#ifndef FOP_MOVE_OBJECT_TO_TARGET_HH_
#define FOP_MOVE_OBJECT_TO_TARGET_HH_

#include <football_op/follow.hh>
#include <football_op/util.hh>
#include <football_op/kick.hh>

namespace fop {

template <typename Robot, typename Threshold, typename Precision>
bool moveBallToTarget(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        const cv::Point_<Precision>& goalOffsetRad,
        const Precision& ballRealRadius,
        int& status,
        const Precision& power = 12
);

} /// namespace fop

namespace fop {

template <typename Robot, typename Threshold, typename Precision>
bool moveBallToTarget(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        const cv::Point_<Precision>& goalOffsetRad,
        const Precision& ballRealRadius,
        int& status,
        const Precision& power
) {
    constexpr static auto VEL_LIN_X = 0.3;
    constexpr static auto VEL_LIN_Y = 0.2;
    constexpr static auto VEL_ANG_YAW = 0.1;
    cv::Point_<Precision> ballPosition;
    Precision ballImageRadius;
    if (status == 0 && !followBall(
            robot,
            trans,
            hLow,
            sLow,
            vLow,
            hHigh,
            sHigh,
            vHigh,
            ballRealRadius,
            ballPosition,
            ballImageRadius
    )) {
        return false;
    } else {
        status++;
    }
    if (status == 1 && !turnAroundBall(
            robot,
            goalOffsetRad,
            calculateOffsetRad(
                    ballPosition,
                    cv::Point_<Precision>{ trans.cols / 2, trans.rows / 2, },
                    robot.horizontalFov(),
                    robot.pan(),
                    robot.verticalFov(),
                    robot.tilt()
            ),
            detail::calculateDistanceToObject(
                    ballImageRadius,
                    ballRealRadius,
                    robot.focusLengthPx()
            ),
            VEL_LIN_X,
            VEL_LIN_Y,
            VEL_ANG_YAW
    )) {
        return false;
    } else {
        status++;
    }
    if (status == 2 && !kickBall(
            robot,
            calculateOffsetRad(
                    ballPosition,
                    cv::Point_<Precision>{ trans.cols / 2, trans.rows / 2, },
                    robot.horizontalFov(),
                    robot.pan(),
                    robot.verticalFov(),
                    robot.tilt()
            ),
            power,
            robot.horizontalFov() / 9
    )) {
        return false;
    } else {
        status++;
    }
    return true;
}

} /// namespace fop

#endif /// FOP_MOVE_OBJECT_TO_TARGET_HH_
