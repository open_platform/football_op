#ifndef FOP_RECTANGLE_HH_
#define FOP_RECTANGLE_HH_

#include <football_op/area.hh>

namespace fop {

template <typename Precision>
class Rectangle : public Area<Precision> {
public:
    constexpr Rectangle() = default;
    constexpr Rectangle(Precision _x, Precision _y);
    virtual bool valid(Precision _x, Precision _y) const override;
protected:
    //! half length
    double x_ = 0.;
    //! half width
    double y_ = 0.;
private:
};

} /// namespace fop

namespace fop {

template <typename Precision>
constexpr Rectangle<Precision>::Rectangle(Precision _x, Precision _y)
    : x_{_x}, y_{_y} {}

template <typename Precision>
bool Rectangle<Precision>::valid(Precision _x, Precision _y) const {
    return
            _x <= this->x_ && _x >= -this->x_ &&
            _y <= this->y_ && _y >= -this->y_;
}

} /// namespace fop

#endif /// FOP_RECTANGLE_HH_
