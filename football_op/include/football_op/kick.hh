#ifndef FOP_KICK_HH_
#define FOP_KICK_HH_

#include <football_op/search.hh>

namespace fop {

/*!
 * @brief tries to find and kick a ball with a robot
 * @param robot is the robot to kick a ball
 * @param trans is the data from optical sensor
 * @param hLow is the low hue threshold for ball detection
 * @param sLow is the low saturation threshold for ball detection
 * @param vLow is the low value threshold for ball detection
 * @param hHigh is the high hue threshold for ball detection
 * @param sHigh is the high saturation threshold for ball detection
 * @param vHigh is the high value threshold for ball detection
 * @param ballRealRadius is the radius [m] of a ball
 * @return true if ball was kicked and false else
 */
template <typename Robot, typename Threshold, typename Precision>
bool kickBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        Precision ballRealRadius
);

/*!
 * @brief kicks a ball with given offset with a robot to a specified distance
 * @param robot is the robot to kick a ball
 * @param ballOffset is the offset of ball to be kicked
 * @param distance is the distance which a ball should cover after a kick
 * @param kickDistanceCoef is the coefficient used to calculate force of a kick
 * @return true if ball was kicked and false else
 */
template <typename Robot, typename Precision>
bool kickBall(
        Robot& robot,
        const cv::Point_<Precision>& ballOffset,
        Precision distance,
        Precision kickDistanceCoef
);

/*!
 * @brief kicks a ball with given offset with a robot
 * @param robot is the robot to kick a ball
 * @param ballOffset is the offset of ball to be kicked
 * @return true if ball was kicked and false else
 */
template <typename Robot, typename Precision>
bool kickBall(
        Robot& robot,
        const cv::Point_<Precision>& ballOffset
);

} /// namespace fop

namespace fop {

template <typename Robot, typename Precision>
bool kickBall(
        Robot& robot,
        const cv::Point_<Precision>& ballOffset,
        int force,
        const Precision& requiredBallOffsetRad,
        int minForce = 6,
        int maxForce = 12
) {
    const auto BALL_OFFSET_EPSILON = requiredBallOffsetRad / 5;
    if (
            ballOffset.x > -requiredBallOffsetRad - BALL_OFFSET_EPSILON
            &&
            ballOffset.x < requiredBallOffsetRad + BALL_OFFSET_EPSILON
    ) {
        if (
                ballOffset.x > -requiredBallOffsetRad + BALL_OFFSET_EPSILON
                &&
                ballOffset.x < requiredBallOffsetRad - BALL_OFFSET_EPSILON
        ) { /// robot should adjust to a ball
            if (ballOffset.x < 0) {
                robot.velocity(0, -robot.velocityLinearLateralMax(), 0);
            } else {
                robot.velocity(0, +robot.velocityLinearLateralMax(), 0);
            }
            return false;
        } else {
            robot.velocity(0, 0, 0);
            if (force < minForce) {
                force = minForce;
            } else if (force > maxForce) {
                force = maxForce;
            }
            if (ballOffset.x < 0) {
                robot.kickLeft(force); /// left
            } else if (ballOffset.x > 0) {
                robot.kickRight(force); /// right
            }
            return true;
        }
    } else {
        if (ballOffset.x < 0) {
            robot.velocity(0, +robot.velocityLinearLateralMax(), 0);
        } else {
            robot.velocity(0, -robot.velocityLinearLateralMax(), 0);
        }
        return false;
    }
}

template <typename Robot, typename Precision>
bool kickBall(
        Robot& robot,
        const cv::Point_<Precision>& ballOffset,
        Precision distance,
        Precision kickDistanceCoef
) {
    /// coef connects the kick force and the distance
    return kickBall(
            robot,
            ballOffset,
            static_cast<int>(kickDistanceCoef * distance),
            robot.horizontalFov() / 9
    );
}

template <typename Robot, typename Precision>
bool kickBall(
        Robot& robot,
        const cv::Point_<Precision>& ballOffset
) {
    return kickBall(
            robot,
            ballOffset,
            12,
            robot.horizontalFov() / 9
    );
}

template <typename Robot, typename Threshold, typename Precision>
bool kickBall(
        Robot& robot,
        const cv::Mat& trans,
        Threshold hLow,
        Threshold sLow,
        Threshold vLow,
        Threshold hHigh,
        Threshold sHigh,
        Threshold vHigh,
        Precision ballRealRadius
) {
    auto ballPosition = cv::Point_<Precision>{};
    auto ballImageRadius = Precision{};
    if (searchBallColorInFront(
            robot,
            trans,
            ballPosition,
            ballImageRadius,
            hLow,
            sLow,
            vLow,
            hHigh,
            sHigh,
            vHigh
    )) {
        const auto ballOffset = calculateOffsetRad(
                ballPosition,
                cv::Point_<Precision>{ trans.cols / 2, trans.rows / 2, },
                robot.horizontalFov(),
                robot.pan(),
                robot.verticalFov(),
                robot.tilt()
        );
        if (kickBall(
                robot,
                ballOffset
        )) {
            return true;
        }
    }
    return false;
}

} /// namespace fop

#endif /// FOP_KICK_HH_
