#ifndef FOP_UTIL_HH_
#define FOP_UTIL_HH_

#include <cmath>

#include <opencv2/opencv.hpp>

namespace fop {

template <typename Precision>
constexpr Precision distanceSquared(
        cv::Vec<Precision, 2> pointLhs,
        cv::Vec<Precision, 2> pointRhs
) {
    return (
            (pointLhs[0] - pointRhs[0]) * (pointLhs[0] - pointRhs[0]) +
            (pointLhs[1] - pointRhs[1]) * (pointLhs[1] - pointRhs[1])
    );
}

/*!
 * @brief calculate offset given the ball position in the image
 *     and the center of the image
 *
 * @tparam Precision is the floating value type
 * @tparam cv::Point_<Precision> type of the ball position
 * @tparam cv::Point_<Precision> type of the image center point
 * @param position the ball position in the image
 * @param center the image center point
 * @param horizontalFov is the field of view in horizontal plane
 * @param pan is the angle of current pan
 * @param horizontalFov is the field of view in vertical plane
 * @param tilt is the angle of current tilt
 * @return offset in radians
 */
template <typename Precision>
constexpr cv::Point_<Precision> calculateOffsetRad(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        Precision horizontalFov,
        Precision pan,
        Precision verticalFov,
        Precision tilt
);

template <typename Robot, typename Precision>
constexpr bool turnAroundBall(
        Robot& robot,
        const cv::Point2f& targetOffset,
        const cv::Point2f& ballOffset,
        const Precision& distanceToBall,
        Precision velLinX,
        Precision velLinY,
        Precision velAngYaw
);

namespace detail {

template <typename Precision = double>
constexpr Precision calculateOffsetXPx(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center
) {
    return position.x - center.x;
}

template <typename Precision>
constexpr Precision calculateOffsetXRad(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        Precision horizontalFov,
        Precision pan
) {
    return calculateOffsetXPx(position, center) /
            center.x * horizontalFov / 2 + pan;
}

template <typename Precision>
constexpr Precision calculateOffsetXM(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        const Precision& distance,
        Precision horizontalFov,
        Precision pan
) {
    return distance * std::sin(calculateOffsetXRad(
            position,
            center,
            horizontalFov,
            pan
    ));
}

template <typename Precision = double>
constexpr Precision calculateOffsetYPx(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center
) {
    return -(position.y - center.y);
}

template <typename Precision>
constexpr Precision calculateOffsetYRad(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        Precision verticalFov,
        Precision tilt
) {
    return calculateOffsetYPx(position, center) /
            center.y * verticalFov / 2 + tilt;
}

template <typename Precision>
constexpr Precision calculateOffsetYM(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        const Precision& distance,
        Precision verticalFov,
        Precision tilt
) {
    return distance *
            std::sin(M_PI / 2 + calculateOffsetYRad(
                    position,
                    center,
                    verticalFov,
                    tilt
            ));
}

template <typename Precision>
constexpr cv::Point_<Precision> calculateOffset(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        Precision horizontalFov,
        Precision pan
) {
    return calculateOffsetRadPx(position, center, horizontalFov, pan);
}

template <typename Precision = double>
constexpr cv::Point_<Precision> calculateOffsetPx(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center
) {
    return cv::Point_<Precision>(
        calculateOffsetXPx(position, center),
        calculateOffsetYPx(position, center)
    );
}

/*!
 * @brief calculate distance to the object from the camera
 * using a focal distance [px]
 *
 * @tparam Precision is the floating value type
 * @param sizePx is the one dimensional half size of the object in the image
 * @return distance to object
 */
template <typename Precision>
constexpr Precision calculateDistanceToObject(
        Precision sizePx,
        Precision sizeReal,
        Precision focusLengthPx
) {
  const Precision coef = focusLengthPx * sizeReal;
  return coef / sizePx;
}

template <typename Precision>
constexpr cv::Point_<Precision> calculateOffsetM(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        Precision sizePx,
        Precision sizeReal,
        Precision focusLengthPx,
        Precision horizontalFov,
        Precision pan,
        Precision verticalFov,
        Precision tilt
) {
    const Precision distance =
            calculateDistanceToObject(sizePx, sizeReal, focusLengthPx);
    return cv::Point_<Precision>(
        calculateOffsetXM(position, center, distance, horizontalFov, pan),
        calculateOffsetYM(position, center, distance, verticalFov, tilt)
    );
}

template <typename Precision>
constexpr cv::Point_<Precision> calculateOffsetRadPx(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        Precision horizontalFov,
        Precision pan
) {
    return cv::Point_<Precision>(
        calculateOffsetXRad(position, center, horizontalFov, pan),
        calculateOffsetYPx(position, center)
    );
}

} /// namespace detail

template <typename Precision>
constexpr cv::Point_<Precision> calculateOffsetRad(
        const cv::Point_<Precision>& position,
        const cv::Point_<Precision>& center,
        Precision horizontalFov,
        Precision pan,
        Precision verticalFov,
        Precision tilt
) {
    return cv::Point_<Precision>{
        detail::calculateOffsetXRad(position, center, horizontalFov, pan),
        detail::calculateOffsetYRad(position, center, verticalFov, tilt)
    };
}

template <typename Robot, typename Precision>
constexpr bool turnAroundBall(
        Robot& robot,
        const cv::Point2f& targetOffset,
        const cv::Point2f& ballOffset,
        const Precision& distanceToBall,
        Precision velLinX,
        Precision velLinY,
        Precision velAngYaw
) {
    /// allowed deviation of a target
    const double eps = robot.horizontalFov() / 16;
    /// allowed deviation of a ball
    const double ballDeviation = robot.horizontalFov() / 16;
    if (targetOffset.x > -eps && targetOffset.x < eps) {
        if (
                ballOffset.x > -ballDeviation &&
                ballOffset.x < ballDeviation
        ) {
            if (distanceToBall <= robot.footKickedLength()) {
                /// stop robot
                robot.velocity(0, 0, 0);
                return true; /// exit with true
            } else {
                /// keep moving robot towards ball
                robot.velocity(velLinX, 0, 0);
            }
        } else if (ballOffset.x < 0) {
            robot.velocity(0, velLinY, 0);
        } else if (ballOffset.x > 0) {
            robot.velocity(0, -velLinY, 0);
        }
    } else if (targetOffset.x < 0) {
        if (
                ballOffset.x > -ballDeviation &&
                ballOffset.x < ballDeviation
        ) {
            robot.velocity(0, 0, velAngYaw);
        } else if (ballOffset.x < 0) {
            robot.velocity(0, velLinY, 0);
        } else if (ballOffset.x > 0) {
            robot.velocity(0, -velLinY, 0);
        }
    } else if (targetOffset.x > 0) {
        if (
                ballOffset.x > -ballDeviation &&
                ballOffset.x < ballDeviation
        ) {
            robot.velocity(0, 0, -velAngYaw);
        } else if (ballOffset.x < 0) {
            robot.velocity(0, velLinY, 0);
        } else if (ballOffset.x > 0) {
            robot.velocity(0, -velLinY, 0);
        }
    }
    return false;
}

} /// namespace fop

#endif /// FOP_UTIL_HH_
