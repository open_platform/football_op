#!/bin/bash

# USE THIS AT YOUR OWN RISK

# sudo apt purge libopencv*
sudo apt install ros-noetic-cv-bridge -y
mkdir ~/lib/opencv/ -p && cd ~/lib/opencv/
git clone https://github.com/opencv/opencv.git -b "4.5.2" --single-branch # opencv main part
git clone https://github.com/opencv/opencv_contrib.git -b "4.5.2" --single-branch # opencv part with xfeatures2d
mkdir build/ && cd build/
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D OPENCV_GENERATE_PKGCONFIG=ON -D OPENCV_ENABLE_NONFREE=ON -D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules -D CMAKE_CXX_STANDARD=11 ../opencv
make -j 8 -l 8
sudo make install
sudo apt install ros-noetic-effort-controllers -y # in case of controller could not be loaded
sudo apt install ros-noetic-joy -y # needed lib for controlling robot
sudo apt install python python-yaml
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1
